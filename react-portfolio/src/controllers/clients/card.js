import React from 'react';

const Card = ({property}) => {
    const {index, picture, content} = property
    return(
        <div id={`card-${index}`} className="card">
            <img src={picture} alt={content}/>
            <div className="details">
                <span className="index">{index+1}</span>
            </div>
        </div>
    )
}

export default Card;
