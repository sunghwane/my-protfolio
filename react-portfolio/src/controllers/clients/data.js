const data = {
        "properties":[
         {
             "_id": 1,
             "index": 0,
             "picture": "../../images/br/apple.png",
             "content":"Apple"
         },
         {
            "_id": 2,
            "index": 1,
            "picture": "../../images/br/avengers.svg",
            "content":"Avengers"
        },
        {
            "_id": 3,
            "index": 2,
            "picture": "../../images/br/disneyland.svg",
            "content":"Disneyland"
        },
        {
            "_id": 4,
            "index": 3,
            "picture": "../../images/br/hyundai.svg",
            "content":"Hyundai"
        },
        {
            "_id": 5,
            "index": 4,
            "picture": "../../images/br/lg.svg",
            "content":"Lg"
        },
        {
            "_id": 6,
            "index": 5,
            "picture": "../../images/br/mozilla.svg",
            "content":"Mozilla"
        },
        {
            "_id": 7,
            "index": 6,
            "picture": "../../images/br/pixar.svg",
            "content":"Pixar"
        },
        {
            "_id": 8,
            "index": 7,
            "picture": "../../images/br/ps.svg",
            "content":"Ps"
        },
        {
            "_id": 9,
            "index": 8,
            "picture": "../../images/br/sampsung.svg",
            "content":"Samsung"
        },
        {
            "_id": 10,
            "index": 9,
            "picture": "../../images/br/universer.svg",
            "content":"Unvierser"
        }
    ]   
}



export default data;