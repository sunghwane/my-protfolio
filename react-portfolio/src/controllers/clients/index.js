import React, {Component} from 'react';
import './hobbies.scss';
import Card from './card';
import data from './data'

export default class Clients extends Component{
    constructor(props){
        super(props)
        this.state = {
            properties: data.properties,
            property: data.properties[4]
        }
    }

    nextProperty = () => {
        const newIndex = this.state.property.index+1;
        this.setState({
            property: data.properties[newIndex]
        });
    }

    prevProperty = () => {
        const newIndex = this.state.property.index-1;
        if(this.state.property.index === 0){
            return 0;
        }
        this.setState({
            property: data.properties[newIndex]
        });
    }


    render(){
        const {properties, property} = this.state;

        return(
            <div className="App inner-box">
                <div className="page">
                    <section>
                        <h1>My Clients</h1>
                    </section>
                    <div className="col">
                        <div className={`cards-slider active-slide-${property.index}`}>
                            <div className="cards-slider-wrapper" style={{
                            "transform": `translateX(-${property.index*(100/properties.length)}%)` 
                            }}>
                                {
                                    properties.map(property => <Card key={property._id} property={property}/>)
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="button-box">
                    <button
                        onClick={() => this.prevProperty()}
                        disabled={property.inedx === 0}>Prev</button>
                    <button
                        onClick={() => this.nextProperty()}
                        disabled={property.index ===
                        data.properties.length-1}>Next-></button>
                </div>
            </div>
        );
    }
}