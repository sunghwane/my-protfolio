import React, {Component} from 'react';
import './myservices.css';

export default class MyServices extends Component{
    render(){
        return(
            <div className="article-box">
        <div className="inner-box">
            <h2 className="section-title">My Services</h2>
            <p className="section-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>

            <ul className="service-list">
                <li>
                    <span className="ico html"></span>
                    <h3>HTML/CSS</h3>
                    <hr />
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque ad facere at expedita. Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
                <li>
                    <span className="ico javascript"></span>
                    <h3>JAVASCRIPT</h3>
                    <hr />
                    <p>
                        Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
                <li>
                    <span className="ico angular"></span>
                    <h3>ANGULAR, RECT, VUE.js</h3>
                    <hr />
                    <p>
                        Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
                <li>
                    <span className="ico java"></span>
                    <h3>JAVA</h3>
                    <hr />
                    <p>
                        Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
                <li>
                    <span className="ico python"></span>
                    <h3>PYTHON</h3>
                    <hr />
                    <p>
                        Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
                <li>
                    <span className="ico database"></span>
                    <h3>DATABASE</h3>
                    <hr />
                    <p>
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cumque ad facere at expedita. Fugiat odit, consequuntur sunt veniam quo commodi architecto iusto. Rerum aliquam aperiam nulla ea esse reiciendis ipsum.
                    </p>
                </li>
            </ul>
        </div>
    </div>
        )
    }
}