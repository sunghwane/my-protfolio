import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Animation from './animation';
import "./home.css";

export default class Home extends Component{
    constructor(props){
        super(props);
        this.state = {date: new Date()};
        }
    componentDidMount() {
        this.timerID = setInterval(
          () => this.tick(),
          1000
        );
      }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
          date: new Date()
        });
    }
    render(){
        return(
            <div className="title-box">
                <div className="slogan-box flexbox">
                    <strong>Welcome! {this.state.date.toLocaleTimeString()}</strong>
                    <Animation/>
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <Home/>,
    document.getElementById("root")
);