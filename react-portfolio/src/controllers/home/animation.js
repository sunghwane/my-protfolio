import React ,{Component} from "react";

const styles = {
    transition: "all 1s ease-out"
};

export default class Animation extends Component{
    constructor(){
        super();
        this.state = {
            opacity:0,
            scale:1,
        }
    }

    onHide(){
        this.setState({
            opacity: 0
        });
    }

    onScale(){
        this.setState({
            opacity: 1,
            scale: this.state.scale > 1 ? 1 : 1.3
        });
    }

    render(){
        return(
            <div>
                <div style={{...styles, opacity: this.state.opacity, transform: "scale("+this.state.scale + ")"}}>
                    <h2>YunSeonghwan Portfolio</h2>
                    <p>제가 만든 사이트입니다 잘 부탁드립니다.</p>
                </div>
                <div>
                    <a className="hide" onClick={this.onHide.bind(this)} style={{cursor: "pointer"}}>Hide</a>
                    <a className="scale" onClick={this.onScale.bind(this)} style={{cursor: "pointer"}}>Scale</a>
                </div>
            </div>   
        )
    }
}
