import React, {Component} from 'react';
import './about.css';

const styles = {
    transition: "all 2s ease-out"
}

export default class About extends Component{
    constructor(){
        super();
        this.state = {
            opacity:0,
        }
    }

    onClick(){
        this.setState({
            opacity: 1,
        });
    }
    

    render(){
        return(
            <div className="introduce-box">
                <div className="inner-box flexbox" style={{...styles, opacity: this.state.opacity}}>
                    <div className="my-picture-box">
                    <a><div 
                    style={{...styles, transform: "scale("+this.state.scale + ")"}}
                    className="picture"></div></a>
                    </div>
                    <div className="my-introduce">
                        <h2 className="section-title">About Me</h2>
                        <h3 className="sub-title">안녕하세요 윤성환 입니다.</h3>
                        <p>안녕하세요 저는 윤성환 이라고 합니다. 나이는 26살이고 서울시 마포구에 거주하고 있습니다. 지금 개발자가 되기위해 준비 중이고 지금 이거는 제가 만든 자기소개서 사이트 입니다 우연히 서점에서 책을 보다 컴퓨터 관련 언어 책을 보게되고 공부를 시작했습니다. 공부를 하는데 지금 적성에 맞고 재미있어서 계속 찾아보면서 공부하고 있는중입니다. 아직 부족한거는 많지만 저는 계속 빠르게 성장해 나갈수 있다고 생각합니다. 앞으로도 변하는 트렌드에 맞춰서 계속 공부하고 배울것입니다. 제가  할줄 언어는 Java, Paython, Html/Css, javascript, node.js 등이있고 프레임워크는 React, Angular를 사용해봤습니다 Angular을 쓰면서 Typescript도 사용해본 경험이있습니다.잘부탁 드립니다.</p>
                    </div>
                </div>
                <a className="btn flexbox" onClick={this.onClick.bind(this)} style={{cursor:"pointer"}}>Click!!</a>
            </div>
        )
    }
}