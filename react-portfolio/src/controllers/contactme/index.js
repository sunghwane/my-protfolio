import React, {Component} from 'react';
import './contactme.css';
import axios from 'axios';

export default class About extends Component{
    constructor(){
        super()

        this.state = {
            name: '',
            email: '',
            title: '',
            message: ''
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value})
    };

    async handleSubmit(e){
        e.preventDefault()
        alert("이메일이 발송 되었습니다.");

        const{name, email, title, message} = this.state

        const form = await axios.post('/api/form', {
            name,
            email,
            title,
            message
        })
    };


    render(){
        return(
            <div className="article-box">
               <div className="inner-box">
                   <h2 className="section-title">Contact Me</h2>
                   <p className="section-description">
                        나에게 연락을 하기위한 다양한 방법들이 있지만<br/>
                        아래의 폼을 채워 전송하면 저는 당신이 보낸 이메일을 확인할 수 있습니다.
                   </p>
                   <ul className="address-list flexbox">
                        <li>
                            <strong>Email me At</strong>
                            <span>baylee.zemlak@ethereal.email</span>
                        </li>
                        <li>
                            <strong>Visit Home</strong>
                            <span>서울시 마포구 동교로</span>
                        </li>
                   </ul>

                   <form onSubmit={this.handleSubmit} className="contact-form">
                       <fieldset>
                           <legend className="txt-only">이메일 입력란</legend>
                           <div>
                               <label>
                                   <span className="txt-only">이메일 보내는 사람 이름</span>
                                   <input id="userName" type="text" name="name" placeholder="Your name*" required onChange={this.handleChange}/>
                               </label>
                               <label>
                                    <span className="txt-only">보내는 사람 이메일</span>
                                    <input id="userName" type="text" name="email" placeholder="Your email*" required onChange={this.handleChange}/>
                                </label>
                           </div>
                           <div>
                                <label>
                                    <span className="txt-only">이메일 제목</span>
                                    <input type="text" name="title" placeholder="Email Title*" required onChange={this.handleChange} />
                                </label>
                            </div>
                            <div>
                                <label>
                                    <span className="txt-only">이메일 내용</span>
                                    <textarea placeholder="Input your message" name="message" onChange={this.handleChange}></textarea>
                                </label>
                            </div>
                            <div className="tac">
                                <input type="submit" value="Send Message" className="btn orange lg round"/>
                            </div>
                       </fieldset>
                   </form>
               </div>
            </div>
        )
    }
}