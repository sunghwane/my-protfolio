import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {MenuList} from '../configuration/menu-list';
import './nav.css';

export default class Navigation extends Component{
    render(){
        const navList = MenuList.map((item) => (
            <li key={item.id}>
                <Link to={item.path}>
                    {item.title}
                </Link>
            </li>
        ));

        return(
            <nav className="gnb">
                <ul className="a">
                    {navList}
                </ul>
            </nav>
        )
    }
}