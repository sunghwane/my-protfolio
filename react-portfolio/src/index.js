import React from 'react';
import ReactDOM from 'react-dom';
import Root from './core/component/root';

ReactDOM.render(<Root/>, document.getElementById("root"));