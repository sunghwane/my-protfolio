import Home from '../controllers/home/index';
import About from '../controllers/about/index';
import Clients from '../controllers/clients/index';
import ContactMe from '../controllers/contactme/index';
import MyServices from '../controllers/myservices/index';
import Portfolio from '../controllers/portfolio/index';

export const MenuList = [
    {id: 1, path: '/', title: 'Home', component: Home, exact: true},
    {id: 2, path: '/about', title: 'About', component: About, exact: false},
    {id: 3, path: '/clients', title: 'Clients', component: Clients, exact: false},
    {id: 4, path: '/contactme', title: 'ContactMe', component: ContactMe, exact: false},
    {id: 5, path: '/myservices', title: 'MyServices', component: MyServices, exact: false},
    {id: 6, path: '/portfolio', title: 'Portfolio', component: Portfolio, exact: false}
]