import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {MenuList} from '../../configuration/menu-list';
import Navigation from '../../components/navigation';
import './App.css';

class App extends Component{
    render(){
        const routerList = MenuList.map((item) => (
            <Route key={item.id}
                path={item.path}
                component={item.component}
                exact={item.exact}/>
        ));

        return(
            <React.Fragment>
                <header className="fixed-full">
                    <div className="inner-box flexbox header-box">
                        <h1>
                            <span>Yun Seonghwan</span>
                        </h1>
                        <nav className="gnb">
                            <Navigation/>
                        </nav>
                    </div>
                </header>
                <div>
                    {routerList}
                </div>
            </React.Fragment>
        )
    }
}

export default App;